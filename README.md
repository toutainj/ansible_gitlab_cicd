# Ansible : Demonstration déploiement d'une infra docker avec AWX, gitlab-ce, gitlab-runner (CI/CD), un reverse proxy nginx et un project Python basé sur le framework web Flask

## Installation des prérequis

Vous devez avoir ansible sur la machine "controleur" ansible

```
sudo apt install python3-pip
pip install ansible
```

## Utilisation

Adapter le fichier "inventory" à la machine où vous souhaitez déployer l'infrastructure.

Déployer avec la commande :

```
ansible-playbook -i inventory playbook.yml --ask-become-pass
```

Vous serez invité à saisir votre mot de passe sudo de votre machine local afin de gérer le fichier /etc/hosts en l'abscence de DNS et/ou nom de domaine "public"