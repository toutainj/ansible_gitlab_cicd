from flask import request, Flask
import json
app = Flask(__name__)
@app.route('/plus_un')
def plus_un():
    x = int(request.args.get('x', 1))
    return json.dumps({'x': x + 1})
@app.route('/carre')
def carre():
    x = int(request.args.get('x', 1))
    return json.dumps({'x': x * x})