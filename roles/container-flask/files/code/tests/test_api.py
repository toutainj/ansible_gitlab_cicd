from urllib.parse import urlencode
import json
def call(client, path, params):
    url = path + '?' + urlencode(params)
    response = client.get(url)
    return json.loads(response.data.decode('utf-8'))
def test_plus_un(client):
    result = call(client, '/plus_un', {'x': 2})
    assert result['x'] == 3
def test_carre(client):
    result = call(client, '/carre', {'x': 2})
    assert result['x'] == 4